# MANDATORY VARIABLES
# ============================================================================

# BASICS
variable "project_id" {
  description = "The Project ID of the project in which the cluster will be created"
}
variable "region" {
  description = "The region in which to create the cluster"
}
variable "environment" {
  description = "A name of the environment which is appended to the cluster name - e.g. dev, sandbox, production"
}
variable "k8s_version_prefix" {
  description = "The version family of Kubernetes to use, e.g. 1.16"
}

# CLUSTER SERVICE ACCOUNT
variable "cluster_sa_name" {
  description = "The name to give to the minimally privileged Service Account used to create and run the nodes"
}
variable "cluster_sa_roles" {
  description = "The roles to assign to the minimally privileged Service Account"
  default = []
}

# NETWORKING
variable "vpc_network" {
  description = "The name of the VPC to create or use for the cluster networking"
}
variable "vpc_subnetwork" {
  description = "The name of the subnet to create or use for the cluster's private network"
}

# NODEPOOL SIZING AND CONFIG
variable "nodepool_name" {
  default = "main"
  description = "A name for the nodepool"
}
variable "nodepool_initial_node_count" {
  description = "How many nodes should be created / exist in EACH ZONE OF THE REGION. Setting this value to 1 in a Region with 3 zones will result in 3 nodes being created"
}
variable "nodepool_max_node_count" {
  description = "What is the maximum number of zones that should exis in EACH ZONE OF THE REGION. Setting this value to 2 in a Region with 3 zones will result in upto 6 Nodes in the cluster when fully scaled up"
}
variable "nodepool_machine_type" {
  description = "What VM size should be used for nodes in the nodepool - e.g n1-standard-4"
}
variable "nodepool_disk_size" {
  description = "How big should the VM's of the nodepool's disks be? Remember throughput relates to size. Recommend 10 for a lab environment and 100 for production environments"
}
variable "nodepool_disk_type" {
  description = "The disk type to use for VM's of the nodepool. pd-standard recommended, it is rare and expensive to need pd-ssd"
}
variable "nodepool_use_preemptible" {
  description = "Whether to use preemtible VM's for the node. Special cases like Lab environment and batch / ML work may want cheaper preemtible hosts - but in most other cases this should be false"
}

# OPTIONAL VARIABLES
# ============================================================================

# NETWORKING (CONT.)
variable "pod_network_size" {
  description = "The size of the IP range to create for the pod's VPC-Native networking"
  default = "/16"
}
variable "service_network_size" {
  description = "The size of the IP range to create for the services' VPC-Native networking"
  default = "/20"
}
# Here we we enable the master_authorized_networks functionality, but given current circumstances it is difficult
# to create the right balance of static(ish) ranges without going through a lot of hoops for all users currently
variable "master_authorized_networks_cidr_blocks" {
  type = list(map(string))
  description = "Add blocks for IP ranges that should be allowed to access the public K8s endpoint"
  default = [
    {
      # External network that can access Kubernetes master through HTTPS. Must
      # be specified in CIDR notation.
      cidr_block = "0.0.0.0/0"
      # Field for users to identify CIDR blocks.
      display_name = "default"
    },
  ]
}

# ADDONS WE MIGHT UTILISE AT A LATER DATE
variable "pod_security_policy_enabled" {
  default = false
}
variable "vertical_pod_autoscaling_enabled" {
  default = false
}
variable "workload_identity_disabled" {
  default = true
}
