# TF
# ============================================================================
terraform {
  required_version                = ">= 0.12.26"
}

# INPUT DATA
# ============================================================================
# Get the available GKE versions in the region/zone of the deployment
data "google_container_engine_versions" "versions" {
  location                        = var.region
  version_prefix                  = var.k8s_version_prefix
}
# Get the list of zones available for regional deployment
data "google_compute_zones" "available" {
}
# Get the google project number
data "google_project" "project" {
}

# CREATE THE CLUSTER
# ============================================================================

resource "google_container_cluster" "gke_cluster" {
  ### GENERAL
  provider                        = google-beta
  name                            = "${var.project_id}-${var.environment}"
  
  ### LOCATION
  location                        = var.region
  min_master_version              = data.google_container_engine_versions.versions.latest_node_version
  
  ### INITIAL NODE POOL
  remove_default_node_pool        = true
  initial_node_count              = "1"
  node_version                    = data.google_container_engine_versions.versions.latest_node_version
  
  ### NETWORK
  network                         = var.vpc_network
  subnetwork                      = var.vpc_subnetwork
  ip_allocation_policy {
    cluster_ipv4_cidr_block       = "/16"
    services_ipv4_cidr_block      = "/16"
  }
  
  ### FEATURES
  enable_kubernetes_alpha    = "false"
  cluster_autoscaling {
    enabled = "false"
  }
  # Must be disabled for any cluster running java workloads
  vertical_pod_autoscaling {
    enabled = var.vertical_pod_autoscaling_enabled
  }

  ### LOGGING & MONITORING
  logging_service            = "logging.googleapis.com/kubernetes"
  monitoring_service         = "monitoring.googleapis.com/kubernetes"

  ### SECURITY
  enable_legacy_abac         = "false"
  master_auth {
    client_certificate_config {
      issue_client_certificate = "false"
    }
  }
  pod_security_policy_config {
    enabled = var.pod_security_policy_enabled
  }
  database_encryption {
    state                    = "ENCRYPTED"
    key_name                 = google_kms_crypto_key.etcd-key.self_link
  }
  master_authorized_networks_config {
    dynamic "cidr_blocks" {
      for_each = var.master_authorized_networks_cidr_blocks
      content {
        cidr_block   = cidr_blocks.value.cidr_block
        display_name = cidr_blocks.value.display_name
      }
    }
  }
  # Enable Private Cluster (with Public API Endpoint protected by master_authorized_networks)
  private_cluster_config {
    enable_private_nodes = true
    enable_private_endpoint = false
    master_ipv4_cidr_block = "172.16.0.0/28"
  }
  # Enable Network Policy Part 1, scroll down to the addons_config for part 2
  network_policy {
    enabled  = true
    provider = "CALICO"
  }

  # TODO - Workload Identity - yes or no?

  ### MAINTENANCE
  maintenance_policy {
    daily_maintenance_window {
      start_time = "23:00"
    }
  }
  ### ADDONS
  addons_config {
    cloudrun_config {
      disabled = true
    }

    config_connector_config {
      enabled = false
    }

    dns_cache_config {
      enabled = false
    }

    gce_persistent_disk_csi_driver_config {
      enabled = false
    }

    horizontal_pod_autoscaling {
      disabled = false
    }

    http_load_balancing {
      disabled = true
    }

    istio_config {
      disabled = true
    }

    kalm_config {
      enabled = false
    }
    # Enable Network Policy Part 2, scroll up to main cluster config for part 1
    network_policy_config {
      disabled = false
    }
  }
}

# KMS/key configuration
# ============================================================================
resource random_id key_suffix {
  byte_length = 8
}

resource "google_kms_key_ring" "gke_keyring" {
  name                       = "${var.project_id}-${var.environment}-${random_id.key_suffix.hex}"
  location                   = var.region
}

# Create a Cloud KMS Key for GKE Etcd Encryption/Decryption
resource "google_kms_crypto_key" "etcd-key" {
  name                       = "${var.project_id}-${var.environment}-key-${random_id.key_suffix.hex}"
  key_ring                   = google_kms_key_ring.gke_keyring.self_link
}

# Add first seperately-managed Nodepool for general applications
# ============================================================================

resource "google_container_node_pool" "main" {
  provider                          = google-beta
  name                              = var.nodepool_name
  location                          = var.region
  cluster                           = google_container_cluster.gke_cluster.name
  initial_node_count                = var.nodepool_initial_node_count
  autoscaling {
    min_node_count                  = var.nodepool_initial_node_count
    max_node_count                  = var.nodepool_max_node_count
  }
  management {
    auto_repair                     = true
    auto_upgrade                    = false
  }
  node_config {
    service_account                 = module.gke_service_account.email
    image_type                      = "COS"
    machine_type                    = var.nodepool_machine_type
    labels = {
      environment                   = var.environment
    }
    disk_size_gb                    = var.nodepool_disk_size
    disk_type                       = var.nodepool_disk_type
    preemptible                     = var.nodepool_use_preemptible
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
  lifecycle {
    ignore_changes = [initial_node_count]
  }
}

# SERVICE ACCOUNT FOR GKE
# ============================================================================
# Cluster's Nodepool VM Service Account
module "gke_service_account" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_service_account"
  name                              = var.cluster_sa_name
}
module "gke_service_account_roles" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_iam_member"
  user                              = module.gke_service_account.email
  roles                             = var.cluster_sa_roles
}
# Add KMS permissions to Google's default GKE service account
module "additional_google_gke_iam_roles" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_iam_member"
  user                              = format("service-%s@container-engine-robot.iam.gserviceaccount.com", data.google_project.project.number)
  roles                             = [ "roles/cloudkms.admin", "roles/cloudkms.cryptoKeyEncrypterDecrypter" ]
}
