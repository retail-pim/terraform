resource "google_pubsub_subscription" "pull_subscription" {
  name                                         = var.subscription_name
  topic                                        = var.subscription_topic
  ack_deadline_seconds                         = var.ack_deadline_seconds
  message_retention_duration                   = var.message_retention_duration
  retain_acked_messages                        = var.retain_acked_messages
  expiration_policy {
    ttl                                        = var.expiration_ttl
  }
  labels                                       = var.labels
}