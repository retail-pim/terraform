# MANDATORY VARIABLES
# ============================================================================

variable "name" {
  description = "The name to give to the service account ()"
}

# OPTIONAL VARIABLES
# ============================================================================

variable "description" {
  description = "The description of the custom service account."
  default     = ""
}