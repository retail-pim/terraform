terraform {
  required_version = ">= 0.12.26"
}

# CREATE SERVICE ACCOUNT
# ============================================================================
resource "google_service_account" "service_account" {
  account_id   = var.name
  display_name = var.description
}
