# Random ID to attempt a unique bucket name
resource random_id bucket_suffix {
  byte_length                = 8
}

resource "google_storage_bucket" "image_bucket" {
  name                       = "${var.gcp_bucket_name}-${random_id.bucket_suffix.hex}"
  project                    = var.project
  location                   = var.region
}

resource "google_compute_backend_bucket" "backend-bucket" {
  name                       = var.gcp_bucket_name
  project                    = var.project
  bucket_name                = "${var.gcp_bucket_name}-${random_id.bucket_suffix.hex}"
  enable_cdn                 = var.gcp_bucket_cdn_enabled
}

