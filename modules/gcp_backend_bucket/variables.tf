# MANDATORY VARIABLES
# ============================================================================

variable "gcp_bucket_name" {
  description = "The name to give to the storage bucket - must be unique"
}

# OPTIONAL VARIABLES
# ============================================================================

variable "gcp_bucket_cdn_enabled" {
  description = "Should the Google CDN functionality be enabled for this bucket? (adds cost)"
  default = false
}

