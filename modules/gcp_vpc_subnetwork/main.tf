terraform {
  required_version = ">= 0.12.26"
}

resource "google_compute_subnetwork" "vpc_subnet" {
  name                            = var.subnet_name
  network                         = var.vpc_name
  ip_cidr_range                   = var.ip_cidr_range
  private_ip_google_access        = var.private_ip_google_access
}