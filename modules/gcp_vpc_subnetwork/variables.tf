# MANDATORY VARIABLES
# ============================================================================
variable "subnet_name" {
  description = "The name to give the created subnet"
}

variable "vpc_name" {
  description = "The name of the VPC in which to create the subnetwork"
}

variable "ip_cidr_range" {
  description = "The IP range to use for the subnetwork in CIDR notation e.g. 10.8.0.0/16"
}

variable "private_ip_google_access" {
  description = "Whether to enable access to GCP services on a private subnet"
  default = true
}
