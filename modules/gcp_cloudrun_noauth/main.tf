resource "google_cloud_run_service" "default" {
  name                    = var.application_name
  location                = var.region

  template {
    spec {
      containers {
        image             = var.image
        dynamic "env" {
          for_each = var.env_vars
          content {
            name = env.key
            value = env.value
          }
        }
      }
    }
  }
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location                = google_cloud_run_service.default.location
  project                 = google_cloud_run_service.default.project
  service                 = google_cloud_run_service.default.name

  policy_data             = data.google_iam_policy.noauth.policy_data
}