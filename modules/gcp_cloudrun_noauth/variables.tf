# MANDATORY VARIABLES
# ============================================================================

variable "application_name" {
  description = "A name for the application that is unique within the namespace and region"
}

variable "image" {
  description = "The full path and tag to the image to use - e.g. gcr.io/cloudrun/hello:latest"
}

variable "region" {
  description = "The region to deploy the cloudrun application in"
  default = "europe-north1"
}

# OPTIONAL VARIABLES
# ============================================================================

variable "env_vars" {
  description = "A map of the environment variables to provide to the container"
  type = map
  default = {
  }
}
