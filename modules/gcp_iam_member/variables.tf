# MANDATORY VARIABLES
# ============================================================================

variable "user" {
  description = "The User to assign the role too"
}

variable "roles" {
  description = "The roles that should be applied to the service account"
  type = list
  default = [
    
  ]
}