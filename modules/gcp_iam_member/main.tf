resource "google_project_iam_member" "iam_membership" {
  for_each = toset(var.roles)

  role    = each.value
  member  = "serviceAccount:${var.user}"
}
