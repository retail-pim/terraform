resource "helm_release" "nginx_ingress" {
  chart         = "stable/nginx-ingress"
  name          = "nginx-ingress"
  namespace     = "kube-system"
  force_update  = true
  recreate_pods = true
  reuse_values  = true
}

resource "kubernetes_cluster_role_binding" "tiller" {
  metadata {
    name = "tiller"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

}