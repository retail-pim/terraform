resource "google_storage_bucket" "storage_bucket" {
  name                        = var.bucket_name
  location                    = var.bucket_location
  force_destroy               = var.force_destroy
  storage_class               = var.storage_class
  versioning {
    enabled                   = var.versioning
  }
  uniform_bucket_level_access = var.uniform_bucket_level_access
}