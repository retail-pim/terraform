# MANDATORY VARIABLES
# ============================================================================

variable "bucket_name" {
  description = "The name to give to the storage bucket - must be unique"
}

variable "bucket_location" {
  description = "The location to create the bucket. See https://cloud.google.com/storage/docs/locations"
}

# OPTIONAL VARIABLES
# ============================================================================

variable "storage_class" {
  description = "The type of storage to use for the bucket. See https://cloud.google.com/storage/docs/storage-classes"
  default = "STANDARD"
}

variable "force_destroy" {
  description = "When enabled, allows Terraform to delete a bucket even if it has content"
  default = false
}

variable "versioning" {
  description = "Whether or not to enable object versioning for the contents of the bucket"
  default = false
}

variable "uniform_bucket_level_access" {
  description = "Whether to enable uniform bucket level access which disables ACL's in favour of IAM for controlling authorization"
  default = true
}
