# MANDATORY VARIABLES
# ============================================================================

variable "router_name" {
  description = "The name that should be given to this router"
}

variable "vpc_name" {
  description = "The name of the VPC that the router should be created in"
}
