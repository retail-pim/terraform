# MANDATORY VARIABLES
# ============================================================================

variable "vpc_name" {
  description = "The name to give the VPC"
}

# OPTIONAL VARIABLES
# ============================================================================

variable "auto_create_subnetworks" {
  description = "Whether or not to automatically created subnetworks in all regions"
  default = false
}

variable "delete_default_routes_on_create" {
  description = "Whether to delete existing default routes"
  default = false
}

variable "routing_mode" {
  description = "Whether routing should be GLOBAL or REGIONAL - you are very unlikely to want GLOBAL"
  default = "REGIONAL"
}