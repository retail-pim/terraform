# MANDATORY VARIABLES
# ============================================================================
variable "pubsub_topic_name" {
  description = "The name to give to the topic"
}

# OPTIONAL VARIABLES
# ============================================================================
variable "labels" {
  type = map
  description = "Key value pairs for labels you wish the topic to have. See https://www.terraform.io/docs/configuration-0-11/variables.html#maps"
  default = {
  }
}