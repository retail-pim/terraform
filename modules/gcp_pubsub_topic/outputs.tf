output "pubsub_topic_name" {
  description = "The name of the topic created"
  value       = google_pubsub_topic.topic.name
}