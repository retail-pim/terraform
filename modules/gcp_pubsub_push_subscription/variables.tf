# MANDATORY VARIABLES
# ============================================================================
variable "subscription_name" {
  description = ""
}

variable "subscription_topic" {
  description = ""
}

variable "push_endpoint" {
  description = ""
}


# OPTIONAL VARIABLES
# ============================================================================
variable "ack_deadline_seconds" {
  description = ""
  default = 600
}

variable "message_retention_duration" {
  description = ""
  default = "604800s"
}

variable "retain_acked_messages" {
  description = ""
  default = false
}

variable "expiration_ttl" {
  description = "Empty string is forever, otherwise use a format such as '7600s'"
  default = "" 
}

variable "labels" {
  type = map
  description = "Key value pairs for labels you wish the subscription to have. See https://www.terraform.io/docs/configuration-0-11/variables.html#maps"
  default = {
  }
}

variable "attributes" {
  type = map
  description = "Key value pairs for attributes you wish the subscription to have. See https://www.terraform.io/docs/configuration-0-11/variables.html#maps"
  default = {
  }
}

