# MANDATORY VARIABLES
# ============================================================================
variable "name" {
  description = "The CryptoKey's name. A CryptoKey’s name belonging to the specified Google Cloud Platform KeyRing and match the regular expression [a-zA-Z0-9_-]{1,63}"
}

variable "keyring" {
  description = "The self_link of the Google Cloud Platform KeyRing to which the key belongs."
}

# OPTIONAL VARIABLES
# ============================================================================

# Important - For a GKE Cluster key rotation would mean the entire cluster API endpoint migrates to a new IP and the credentials for the root CA are regenerated.
# Though best practice to rotate it from a security perspective - it would be a very dramatic impact on all related applications, services and CI that interacts with the cluster. 
# Given that this is a huge breaking change that would happen on rotation for anything related to the cluster it is extremely difficult to recommend automatic key rotation for GKE 
# clusters. Leave Empty if the key is to be used for the GKE encryption.
variable "rotation_period" {
  description = "How often the key should be automatically rotated."
  default = ""
}

variable "purpose" {
  description = "The capabilities of the key"
  default = "ENCRYPT_DECRYPT"
}