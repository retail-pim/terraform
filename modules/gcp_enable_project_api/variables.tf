# MANDATORY VARIABLES
# ============================================================================
variable "service" {
  description = "The API to enable. See https://console.cloud.google.com/apis/library for list "
}

# OPTIONAL VARIABLES
# ============================================================================
variable "disable_dependent_services" {
  description = "Should the service that are dependent on this service be disabled on a terraform destroy - likely not."
  default = false
}

variable "disable_on_destroy"{
  description = "Should the API be disabled again on destroy - Likely not"
  default = false
}
