# MANDATORY VARIABLES
# ============================================================================

variable "name" {
  description = "The KeyRing's name. Must match the regular expression [a-zA-Z0-9_-]{1,63}"
}

# OPTIONAL VARIABLES
# ============================================================================
variable "location" {
  description = "The location/region that the keyring should be created in"
  default = "europe-north1"
}
