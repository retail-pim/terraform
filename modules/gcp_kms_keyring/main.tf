# It is not possible to delete keys/keyrings in the normal manner - only to make the inactive. Thus we should randomise the names each time
resource random_id key_suffix {
  byte_length = 8
}

resource "google_kms_key_ring" "kms_keyring" {
  name                       = "${var.name}-${random_id.key_suffix.hex}"
  location                   = var.location
}