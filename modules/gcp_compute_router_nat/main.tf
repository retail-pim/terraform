resource "google_compute_router_nat" "nat" {
  name                               = var.nat_name
  router                             = var.router_name
  nat_ip_allocate_option             = var.nat_ip_allocate_option
  source_subnetwork_ip_ranges_to_nat = var.source_subnetwork_ip_ranges_to_nat

  log_config {
    enable = false
    filter = "ERRORS_ONLY"
  }
}