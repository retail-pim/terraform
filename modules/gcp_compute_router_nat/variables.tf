# MANDATORY VARIABLES
# ============================================================================

variable "nat_name" {
  description = "The name to give to this NAT"
}

variable "router_name" {
  description = "The name of the router in which this NAT should be created"
}

# OPTIONAL VARIABLES (You are unlikely to want to changes these!)
# ============================================================================

variable "nat_ip_allocate_option" {
  description = "Method used to assign NAT IP's - valid values are AUTO_ONLY and MANUAL_ONLY"
  default = "AUTO_ONLY"
}

variable "source_subnetwork_ip_ranges_to_nat" {
  description = "Which IP's are allowed to NAT - valid values are ALL_SUBNETWORKS_ALL_IP_RANGES or ALL_SUBNETWORKS_ALL_PRIMARY_RANGES"
  default = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}