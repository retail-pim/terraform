# Terraform Modules

[[_TOC_]]

## Purpose

Opinionated terraform modules

## How To Use

To utilise a module from this project you will reference the git repo and path to the module as the source

*NOTE* : PLEASE NOTICE THE DOUBLE FORWARD SLASH `//` WHEN REFERENCING A SUBDIRECTORY OF A GIT REPO

1. Declare the module in your terraform file
```hcl
module "my_vpc" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_vpc_network"
  vpc_name                          = "shiny-new-vpc"
}
```

Or for a specific version of the module such as v1.2.9

```hcl
module "my_vpc" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git?v1.2.9//modules/gcp_vpc_network"
  vpc_name                          = "shiny-new-vpc"
}
```

2. Then run terraform init to import the module

3. Make sure you specify all variables listed under MANDATORY VARIABLES in the module's variables.tf file before trying to plan/apply

## Example Environment

In the example-environment directory you will find a simple environment using the modules included in this project. It will be used for testing but also as a reference of how you can create an environment in the project of your choice using these modules

## Testing (Switch from remote to local modules)

### Local Testing

Sometimes, especially when updating multiple, related modules, you may want to switch between local versions of the module and those that are in master branch

Search for: `git::https://gitlab.com/retail-pim/terraform.git//modules/`

Replace with `../modules/`

