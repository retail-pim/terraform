# BASIC INSTRUCTIONS FOR USE
# SEARCH FOR "CHANGE_ME" and replace with appropriate values for the environment you wish to create
# ============================================================================

# TF & PROVIDER CONFIG
# ============================================================================
terraform {
  required_version                = ">= 0.12.26"
}
# Beta provider, used for GKE mostly
provider "google-beta" {
  project                         = "ingka-devops-ersw-dev"  # CHANGE_ME
  region                          = "europe-north1"          # CHANGE_ME
}
# Other than GKE we use the standard provider
provider "google" {
  project                         = "ingka-devops-ersw-dev"  # CHANGE_ME
  region                          = "europe-north1"          # CHANGE_ME
}

# NETWORKING
# ============================================================================
module "rpim_vpc" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_vpc_network"
  vpc_name                          = "rpim-net"
}
# Subnet for GKE to base its vlans on
module "rpim_subnet_gke" {  # CHANGE_ME
  source                          = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_vpc_subnetwork"
  vpc_name                        = module.rpim_vpc.vpc_name
  subnet_name                     = "${module.rpim_vpc.vpc_name}-gke"
  ip_cidr_range                   = "10.8.0.0/16"
  private_ip_google_access        = true
}
# Router and NAT for outbound internet access from the nodes/pods
module "router" {
  source                          = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_compute_router"
  vpc_name                        = module.rpim_vpc.vpc_name
  router_name                     = "${module.rpim_vpc.vpc_name}-router"
}
module "nat" {
  source                          = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_compute_router_nat"
  router_name                     = module.router.router_name
  nat_name                        = "${module.router.router_name}-nat"
}

# GKE CLUSTER & NODEPOOL
# ============================================================================
module "gke_cluster" {
  # BASICS
  project_id                        = "ingka-devops-ersw-dev"  # CHANGE_ME
  region                            = "europe-north1"          # CHANGE_ME
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gke_container_cluster"
  environment                       = "sandbox"                # CHANGE_ME
  k8s_version_prefix                = "1.16"
  # NETWORKING
  vpc_network                       = module.rpim_vpc.vpc_name
  vpc_subnetwork                    = module.rpim_subnet_gke.subnet_name
  pod_network_size                  = "/16"
  service_network_size              = "/16"
  # SECURITY
  pod_security_policy_enabled       = false
  workload_identity_disabled        = true
  # FEATURES
  vertical_pod_autoscaling_enabled  = false
  # NODEPOOL APPS
  nodepool_name                     = "test-nodepool"
  nodepool_initial_node_count       = "1"
  nodepool_max_node_count           = "1"
  nodepool_machine_type             = "n1-standard-1"
  nodepool_disk_size                = "10"
  nodepool_disk_type                = "pd-standard"
  nodepool_use_preemptible          = true
  # SERVICE ACCOUNT
  cluster_sa_name                   = "gke-sa" # Minimum 6 chars
  cluster_sa_roles                  = [
    "roles/logging.logWriter",
    "roles/monitoring.metricWriter",
    "roles/monitoring.viewer",
    "roles/stackdriver.resourceMetadata.writer"
  ]
}

# STORAGE BUCKETS
# ============================================================================
module "test_bucket" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_storage_bucket_no_logs"
  bucket_name                       = "test-bucket-ersw-this-must-be-unique"
  bucket_location                   = "EU"
  force_destroy                     = true
  versioning                        = true
  uniform_bucket_level_access       = true
}
module "test_bucket_with_logs" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_storage_bucket_with_logs"
  bucket_name                       = "test-bucket-ersw-this-must-be-unique-logs"
  bucket_location                   = "EU"
  force_destroy                     = true
  versioning                        = true
  uniform_bucket_level_access       = true
  logging_bucket_name               = "silly-log-bucket"
}

# PUB/SUB
# ============================================================================
module "test_topic" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_pubsub_topic"
  pubsub_topic_name                 = "TEST-TOPIC"
  labels                            = {
    "foo" = "bar"
  }
}

module "test_push_sub" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_pubsub_push_subscription"
  subscription_name                 = "push_sub_test"
  subscription_topic                = module.test_topic.pubsub_topic_name
  push_endpoint                     = "https://example.com/push"
}

module "test_pull_sub" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_pubsub_pull_subscription"
  subscription_name                 = "pull_sub_test"
  subscription_topic                = module.test_topic.pubsub_topic_name
}

# ENABLE PROJECT API'S
# ============================================================================
# GKE 
module "enable_gke_api" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_enable_project_api"
  service                           = "container.googleapis.com"
}
# KMS
module "enable_kms_api" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_enable_project_api"
  service                           = "cloudkms.googleapis.com"
}
# IAM
module "enable_cloud_resource_manager_api" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_enable_project_api"
  service                           = "cloudresourcemanager.googleapis.com"
}
# CLOUDRUN
module "enable_cloudrun_api" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_enable_project_api"
  service                           = "run.googleapis.com"
}
# CLOUDRUN
# ============================================================================
module "test_cloudrun" {
  source                            = "git::https://gitlab.com/retail-pim/terraform.git//modules/gcp_cloudrun_noauth"
  application_name                  = "ersw-test"
  image                             = "gcr.io/cloudrun/hello:latest"
  env_vars                          = {
    "foo" = "bar"
  }
}
