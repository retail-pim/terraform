#!/bin/sh

# Dependencies for Gitlab CI
apk --no-cache add jq
alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"
cd ${TF_ROOT}

# Add Credentials to job
echo "$GCP_INFRA_SA_DEV" > /tmp/creds.json
export GOOGLE_APPLICATION_CREDENTIALS=/tmp/creds.json

# Useful output for job logs
terraform --version

# Initialise the backend bucket
terraform init -backend-config="bucket=sandbox-tf-backend"

